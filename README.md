[![pipeline status](https://gitlab.com/ap-b6-2021/overtale/badges/master/pipeline.svg)](https://gitlab.com/ap-b6-2021/overtale/-/commits/master)
[![coverage report](https://gitlab.com/ap-b6-2021/overtale/badges/master/coverage.svg)](https://gitlab.com/ap-b6-2021/overtale/-/commits/master)

# Overtale
Group Project Advanced Programming - B06

# Anggota
- I Gede Aditya Premana Putra (1906350566)
- Jonathan Amadeus Hartman (1906400261)
- Nunun Hidayah (1806141403)
- Salsabila Adnan (1806186704)
- Sonia Rahmawati (1906398654)

# Deskripsi
Overtale merupakan Text-Based MMORPG Web Game dimana player dapat menjadi salah satu dari 3 class dan melawan monster bersama.

# Pemabgian Tugas
- Front-End (HTML/CSS) - Sonia Rahmawati
- Player Class - Player Stats (Strategy Pattern) - Nunun Hidayah
- Items & Consumables (Adapter Pattern) - Salsabila Adnan
- PvE Monster - Attack Pattern (Template Pattern) -  I Gede Aditya Premana Putra
- Authentication - Jonathan Amadeus Hartman

# Link
- Main Service: https://gitlab.com/ap-b6-2021/overtale
- Battle Service: https://gitlab.com/ap-b6-2021/overtale-battle