package id.ui.cs.overtale.main.core.items.weapons.bow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class UranosBowTest {
    private Class<?> uranosBowClass;
    private UranosBow uranosBow;


    @BeforeEach
    void setUp() throws Exception {
        uranosBowClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.bow.UranosBow");
        uranosBow = new UranosBow("Uranos Bow Holder");
    }

    @Test
    void testUranosBowIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(uranosBowClass.getModifiers()));
    }

    @Test
    void testUranosBowIsABow() {
        Collection<Type> interfaces = Arrays.asList(uranosBowClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.bow.Bow")));
    }

    @Test
    void testUranosBowOverrideShootArrowMethod() throws Exception {
        Class<?>[] shootArrowArgs = new Class[1];
        shootArrowArgs[0] = boolean.class;
        Method shootArrow = uranosBowClass.getDeclaredMethod("shootArrow", shootArrowArgs);

        assertEquals("java.lang.String",
                shootArrow.getGenericReturnType().getTypeName());
        assertEquals(1,
                shootArrow.getParameterCount());
        assertTrue(Modifier.isPublic(shootArrow.getModifiers()));
    }

    @Test
    void testUranosBowOverrideGetNameMethod() throws Exception {
        Method getName = uranosBowClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testUranosBowOverrideGetHolderMethod() throws Exception {
        Method getHolderName = uranosBowClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testUranosBowShootMethod() throws Exception {
        boolean isNotAimShot = false;
        String shootingResult = uranosBow.shootArrow(isNotAimShot);
        assertEquals("headshot!", shootingResult);

        boolean isAimShot = true;
        shootingResult = uranosBow.shootArrow(isAimShot);
        assertEquals("Gaining charge... gaining speed... headshot!", shootingResult);
    }

    @Test
    void testUranosBowGetName() throws Exception {
        String bowName = uranosBow.getName();
        assertEquals("Uranos Bow", bowName);
    }

    @Test
    void testUranosBowGetHolderName() throws Exception {
        String holderName = uranosBow.getHolderName();
        assertEquals("Uranos Bow Holder", holderName);
    }

}
