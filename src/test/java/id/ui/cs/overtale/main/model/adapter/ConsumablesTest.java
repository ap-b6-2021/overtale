package id.ui.cs.overtale.main.model.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConsumablesTest {
    private Consumables consumables;

    @BeforeEach
    void setUp() throws Exception {
        consumables = new Consumables("consumables", 5, 10, 15, 20, 25);
    }

    @Test
    void testConsumablesGetName() throws Exception {
        String consumablesName = consumables.getName();
        assertEquals("consumables", consumablesName);
    }

    @Test
    void testConsumablesGetDefense() throws Exception {
        int consumablesDefense = consumables.getDefense();
        assertEquals(5, consumablesDefense);
    }

    @Test
    void testConsumablesGetAgility() throws Exception {
        int consumablesAgility = consumables.getAgility();
        assertEquals(10, consumablesAgility);
    }

    @Test
    void testConsumablesGetMana() throws Exception {
        int consumablesMana = consumables.getMana();
        assertEquals(15, consumablesMana);
    }

    @Test
    void testConsumablesGetHealthRegen() throws Exception {
        int consumablesHealthRegen = consumables.getHealthRegen();
        assertEquals(20, consumablesHealthRegen);
    }

    @Test
    void testConsumablesGetManaRegen() throws Exception {
        int consumablesManaRegen = consumables.getManaRegen();
        assertEquals(25, consumablesManaRegen);
    }
}
