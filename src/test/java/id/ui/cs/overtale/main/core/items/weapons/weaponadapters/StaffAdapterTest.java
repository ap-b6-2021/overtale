package id.ui.cs.overtale.main.core.items.weapons.weaponadapters;

import id.ui.cs.overtale.main.core.items.weapons.staff.RubyStaff;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class StaffAdapterTest {
    private Class<?> staffAdapterClass;
    private Class<?> staffClass;
    private StaffAdapter staffAdapter;
    private RubyStaff rubyStaff;

    @BeforeEach
    void setUp() throws Exception {
        staffAdapterClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.weaponadapters.StaffAdapter");
        staffClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.staff.Staff");
        rubyStaff = new RubyStaff("Ruby Staff Holder");
        staffAdapter = new StaffAdapter(rubyStaff);
    }

    @Test
    void testStaffAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(staffAdapterClass.getModifiers()));
    }

    @Test
    void testStaffAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(staffAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.weapon.Weapon")));
    }

    @Test
    void testStaffAdapterConstructorReceivesStaffAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = staffClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                staffAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    void testStaffAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = staffAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    void testStaffAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = staffAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    void testStaffAdapterOverrideGetNameMethod() throws Exception {
        Method getName = staffAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testStaffAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = staffAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testStaffAdapterNormalAttack() throws Exception {
        String staffResult = staffAdapter.normalAttack();
        assertEquals(rubyStaff.cast(), staffResult);
    }

    @Test
    void testStaffAdapterChargedAttack() throws Exception {
        String staffResult = staffAdapter.chargedAttack();
        assertEquals(rubyStaff.rechargeMana(), staffResult);
    }

    @Test
    void testStaffAdapterGeName() throws Exception {
        String staffName = staffAdapter.getName();
        assertEquals(rubyStaff.getName(), staffName);
    }

    @Test
    void testStaffAdapterGeHolderName() throws Exception {
        String staffName = staffAdapter.getHolderName();
        assertEquals(rubyStaff.getHolderName(), staffName);
    }
}
