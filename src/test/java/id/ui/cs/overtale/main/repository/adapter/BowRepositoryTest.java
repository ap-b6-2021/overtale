package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.bow.Bow;
import id.ui.cs.overtale.main.core.items.weapons.bow.IonicBow;
import id.ui.cs.overtale.main.core.items.weapons.bow.UranosBow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
class BowRepositoryTest {
    private BowRepository bowRepository;

    @Mock
    private Map<String, Bow> bows;

    private Bow sampleBow;

    @BeforeEach
    void setUp() {
        bowRepository = new BowRepositoryImpl();
        bows = new HashMap<>();
        sampleBow = new IonicBow("Ionic Bow");
        bows.put(sampleBow.getName(), sampleBow);
    }

    @Test
    void whenBowRepoFindAllItShouldReturnBowList() {
        ReflectionTestUtils.setField(bowRepository, "bows", bows);
        List<Bow> acquiredBows = bowRepository.findAll();

        assertThat(acquiredBows).isEqualTo(new ArrayList<>(bows.values()));
    }

    @Test
    void whenBowRepoFindByAliasItShouldReturnBowList() {
        ReflectionTestUtils.setField(bowRepository, "bows", bows);
        Bow acquiredBow = bowRepository.findByAlias(sampleBow.getName());

        assertThat(acquiredBow).isEqualTo(sampleBow);
    }

    @Test
    void whenBowRepoSaveItShouldSaveBow() {
        ReflectionTestUtils.setField(bowRepository, "bows", bows);
        Bow newBow = new UranosBow("Uranos Bow");
        bowRepository.save(newBow);
        Bow acquiredBow = bowRepository.findByAlias(newBow.getName());

        assertThat(acquiredBow).isEqualTo(newBow);
    }
}
