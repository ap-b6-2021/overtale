package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.staff.DiamondStaff;
import id.ui.cs.overtale.main.core.items.weapons.staff.RubyStaff;
import id.ui.cs.overtale.main.core.items.weapons.staff.Staff;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
class StaffRepositoryTest {
    private StaffRepository staffRepository;

    @Mock
    private Map<String, Staff> staffs;

    private Staff sampleStaff;

    @BeforeEach
    void setUp() {
        staffRepository = new StaffRepositoryImpl();
        staffs = new HashMap<>();
        sampleStaff = new DiamondStaff("Diamond Staff");
        staffs.put(sampleStaff.getName(), sampleStaff);
    }

    @Test
    void whenStaffRepoFindAllItShouldReturnStaffList() {
        ReflectionTestUtils.setField(staffRepository, "staffs", staffs);
        List<Staff> acquiredStaffs = staffRepository.findAll();

        assertThat(acquiredStaffs).isEqualTo(new ArrayList<>(staffs.values()));
    }

    @Test
    void whenStaffRepoFindByAliasItShouldReturnStaffList() {
        ReflectionTestUtils.setField(staffRepository, "staffs", staffs);
        Staff acquiredStaff = staffRepository.findByAlias(sampleStaff.getName());

        assertThat(acquiredStaff).isEqualTo(sampleStaff);
    }

    @Test
    void whenStaffRepoSaveItShouldSaveStaff() {
        ReflectionTestUtils.setField(staffRepository, "staffs", staffs);
        Staff newStaff = new RubyStaff("Ruby Staff");
        staffRepository.save(newStaff);
        Staff acquiredStaff = staffRepository.findByAlias(newStaff.getName());

        assertThat(acquiredStaff).isEqualTo(newStaff);
    }
}
