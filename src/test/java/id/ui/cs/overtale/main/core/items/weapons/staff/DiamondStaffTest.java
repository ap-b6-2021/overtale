package id.ui.cs.overtale.main.core.items.weapons.staff;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class DiamondStaffTest {
    private Class<?> diamondStaffClass;
    private DiamondStaff diamondStaff;

    @BeforeEach
    void setUp() throws Exception {
        diamondStaffClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.staff.DiamondStaff");
        diamondStaff = new DiamondStaff("Diamond Staff Holder");
    }

    @Test
    void testDiamondStaffIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(diamondStaffClass.getModifiers()));
    }

    @Test
    void testDiamondStaffIsAStaff() {
        Collection<Type> interfaces = Arrays.asList(diamondStaffClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.staff.Staff")));
    }

    @Test
    void testDiamondStaffOverrideRechargeManaMethod() throws Exception {
        Method rechargeMana = diamondStaffClass.getDeclaredMethod("rechargeMana");

        assertEquals("java.lang.String",
                rechargeMana.getGenericReturnType().getTypeName());
        assertEquals(0,
                rechargeMana.getParameterCount());
        assertTrue(Modifier.isPublic(rechargeMana.getModifiers()));
    }

    @Test
    void testDiamondStaffOverrideCastMethod() throws Exception {
        Method cast = diamondStaffClass.getDeclaredMethod("cast");

        assertEquals("java.lang.String",
                cast.getGenericReturnType().getTypeName());
        assertEquals(0,
                cast.getParameterCount());
        assertTrue(Modifier.isPublic(cast.getModifiers()));
    }

    @Test
    void testDiamondStaffOverrideGetNameMethod() throws Exception {
        Method getName = diamondStaffClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testDiamondStaffOverrideGetHolderMethod() throws Exception {
        Method getHolderName = diamondStaffClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testDiamondStaffRechargeManaMethod() throws Exception {
        String rechargeManaResult = diamondStaff.rechargeMana();
        assertEquals("Diamond Staff recharge mana", rechargeManaResult);
    }

    @Test
    void testDiamondStaffCastMethod() throws Exception {
        String castResult = diamondStaff.cast();
        assertEquals("Diamond Staff cast", castResult);
    }

    @Test
    void testDiamondStaffGetName() throws Exception {
        String staffName = diamondStaff.getName();
        assertEquals("Diamond Staff", staffName);
    }

    @Test
    void testDiamondStaffGetHolderName() throws Exception {
        String holderName = diamondStaff.getHolderName();
        assertEquals("Diamond Staff Holder", holderName);
    }
}