package id.ui.cs.overtale.main.model.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SwordTest {
    private Sword sword;

    @BeforeEach
    void setUp() throws Exception {
        sword = new Sword("sword", 15);
    }

    @Test
    void testSwordGetName() throws Exception {
        String swordName = sword.getName();
        assertEquals("sword", swordName);
    }

    @Test
    void testSwordGetDamage() throws Exception {
        int swordDamage = sword.getDamage();
        assertEquals(15, swordDamage);
    }
}
