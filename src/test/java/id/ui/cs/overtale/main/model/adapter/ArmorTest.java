package id.ui.cs.overtale.main.model.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    private Armor armor;

    @BeforeEach
    void setUp() throws Exception {
        armor = new Armor("armor", 10, 15, 20);
    }

    @Test
    void testArmorGetName() throws Exception {
        String armorName = armor.getName();
        assertEquals("armor", armorName);
    }

    @Test
    void testArmorGetDefense() throws Exception {
        int armorDefense = armor.getDefense();
        assertEquals(10, armorDefense);
    }

    @Test
    void testArmorGetAgility() throws Exception {
        int armorAgility = armor.getAgility();
        assertEquals(15, armorAgility);
    }

    @Test
    void testArmorGetMana() throws Exception {
        int armorMana = armor.getMana();
        assertEquals(20, armorMana);
    }

}
