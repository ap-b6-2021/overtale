package id.ui.cs.overtale.main.core.items.weapons.sword;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class StoneSwordTest {
    private Class<?> stoneSwordClass;
    private StoneSword stoneSword;

    @BeforeEach
    void setUp() throws Exception {
        stoneSwordClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.sword.StoneSword");
        stoneSword = new StoneSword("Stone Sword Holder");
    }

    @Test
    void testStoneSwordIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(stoneSwordClass.getModifiers()));
    }

    @Test
    void testStoneSwordIsAStaff() {
        Collection<Type> interfaces = Arrays.asList(stoneSwordClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.sword.Sword")));
    }

    @Test
    void testStoneSwordOverrideCutMethod() throws Exception {
        Method cut = stoneSwordClass.getDeclaredMethod("cut");

        assertEquals("java.lang.String",
                cut.getGenericReturnType().getTypeName());
        assertEquals(0,
                cut.getParameterCount());
        assertTrue(Modifier.isPublic(cut.getModifiers()));
    }

    @Test
    void testStoneSwordOverrideThrustMethod() throws Exception {
        Method thrust = stoneSwordClass.getDeclaredMethod("thrust");

        assertEquals("java.lang.String",
                thrust.getGenericReturnType().getTypeName());
        assertEquals(0,
                thrust.getParameterCount());
        assertTrue(Modifier.isPublic(thrust.getModifiers()));
    }

    @Test
    void testStoneSwordOverrideGetNameMethod() throws Exception {
        Method getName = stoneSwordClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testStoneSwordOverrideGetHolderMethod() throws Exception {
        Method getHolderName = stoneSwordClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testStoneSwordCutMethod() throws Exception {
        String cutResult = stoneSword.cut();
        assertEquals("Stone Sword cut", cutResult);
    }

    @Test
    void testStoneSwordThrustMethod() throws Exception {
        String thrustResult = stoneSword.thrust();
        assertEquals("Stone Sword thrust", thrustResult);
    }

    @Test
    void testStoneSwordGetName() throws Exception {
        String swordName = stoneSword.getName();
        assertEquals("Stone Sword", swordName);
    }

    @Test
    void testStoneSwordGetHolderName() throws Exception {
        String holderName = stoneSword.getHolderName();
        assertEquals("Stone Sword Holder", holderName);
    }
}