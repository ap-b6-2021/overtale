package id.ui.cs.overtale.main.model.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BowTest {
    private Bow bow;

    @BeforeEach
    void setUp() throws Exception {
        bow = new Bow("bow", 5);
    }

    @Test
    void testBowGetName() throws Exception {
        String bowName = bow.getName();
        assertEquals("bow", bowName);
    }

    @Test
    void testBowGetDamage() throws Exception {
        int bowDamage = bow.getDamage();
        assertEquals(5, bowDamage);
    }

}
