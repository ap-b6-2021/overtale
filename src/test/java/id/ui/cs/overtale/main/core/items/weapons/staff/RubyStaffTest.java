package id.ui.cs.overtale.main.core.items.weapons.staff;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class RubyStaffTest {
    private Class<?> rubyStaffClass;
    private RubyStaff rubyStaff;

    @BeforeEach
    void setUp() throws Exception {
        rubyStaffClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.staff.RubyStaff");
        rubyStaff = new RubyStaff("Ruby Staff Holder");
    }

    @Test
    void testRubyStaffIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(rubyStaffClass.getModifiers()));
    }

    @Test
    void testRubyStaffIsAStaff() {
        Collection<Type> interfaces = Arrays.asList(rubyStaffClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.staff.Staff")));
    }

    @Test
    void testRubyStaffOverrideRechargeManaMethod() throws Exception {
        Method rechargeMana = rubyStaffClass.getDeclaredMethod("rechargeMana");

        assertEquals("java.lang.String",
                rechargeMana.getGenericReturnType().getTypeName());
        assertEquals(0,
                rechargeMana.getParameterCount());
        assertTrue(Modifier.isPublic(rechargeMana.getModifiers()));
    }

    @Test
    void testRubyStaffOverrideCastMethod() throws Exception {
        Method cast = rubyStaffClass.getDeclaredMethod("cast");

        assertEquals("java.lang.String",
                cast.getGenericReturnType().getTypeName());
        assertEquals(0,
                cast.getParameterCount());
        assertTrue(Modifier.isPublic(cast.getModifiers()));
    }

    @Test
    void testRubyStaffOverrideGetNameMethod() throws Exception {
        Method getName = rubyStaffClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testRubyStaffOverrideGetHolderMethod() throws Exception {
        Method getHolderName = rubyStaffClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testRubyStaffRechargeManaMethod() throws Exception {
        String rechargeManaResult = rubyStaff.rechargeMana();
        assertEquals("Ruby Staff recharge mana", rechargeManaResult);
    }

    @Test
    void testRubyStaffCastMethod() throws Exception {
        String castResult = rubyStaff.cast();
        assertEquals("Ruby Staff cast", castResult);
    }

    @Test
    void testRubyStaffGetName() throws Exception {
        String staffName = rubyStaff.getName();
        assertEquals("Ruby Staff", staffName);
    }

    @Test
    void testRubyStaffGetHolderName() throws Exception {
        String holderName = rubyStaff.getHolderName();
        assertEquals("Ruby Staff Holder", holderName);
    }
}
