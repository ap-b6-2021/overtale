package id.ui.cs.overtale.main.core.items.weapons.weaponadapters;

import id.ui.cs.overtale.main.core.items.weapons.sword.IronSword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class SwordAdapterTest {
    private Class<?> swordAdapterClass;
    private Class<?> swordClass;
    private SwordAdapter swordAdapter;
    private IronSword ironSword;

    @BeforeEach
    void setUp() throws Exception {
        swordAdapterClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.weaponadapters.SwordAdapter");
        swordClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.sword.Sword");
        ironSword = new IronSword("Iron Sword Holder");
        swordAdapter = new SwordAdapter(ironSword);
    }

    @Test
    void testSwordAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(swordAdapterClass.getModifiers()));
    }

    @Test
    void testSwordAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(swordAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.weapon.Weapon")));
    }

    @Test
    void testSwordAdapterConstructorReceivesSwordAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = swordClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                swordAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    void testSwordAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = swordAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    void testSwordAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = swordAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    void testSwordAdapterOverrideGetNameMethod() throws Exception {
        Method getName = swordAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testSwordAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = swordAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testSwordAdapterNormalAttack() throws Exception {
        String swordResult = swordAdapter.normalAttack();
        assertEquals(ironSword.cut(), swordResult);
    }

    @Test
    void testSwordAdapterChargedAttack() throws Exception {
        String swordResult = swordAdapter.chargedAttack();
        assertEquals(ironSword.thrust(), swordResult);
    }

    @Test
    void testSwordAdapterGeName() throws Exception {
        String swordName = swordAdapter.getName();
        assertEquals(ironSword.getName(), swordName);
    }

    @Test
    void testSwordAdapterGeHolderName() throws Exception {
        String swordName = swordAdapter.getHolderName();
        assertEquals(ironSword.getHolderName(), swordName);
    }
}
