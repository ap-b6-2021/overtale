package id.ui.cs.overtale.main.core.items.weapons.sword;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class IronSwordTest {
    private Class<?> ironSwordClass;
    private IronSword ironSword;

    @BeforeEach
    void setUp() throws Exception {
        ironSwordClass = Class.forName("id.ui.cs.overtale.main.core.items.weapons.sword.IronSword");
        ironSword = new IronSword("Iron Sword Holder");
    }

    @Test
    void testIronSwordIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ironSwordClass.getModifiers()));
    }

    @Test
    void testIronSwordIsAStaff() {
        Collection<Type> interfaces = Arrays.asList(ironSwordClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ui.cs.overtale.main.core.items.weapons.sword.Sword")));
    }

    @Test
    void testIronSwordOverrideCutMethod() throws Exception {
        Method cut = ironSwordClass.getDeclaredMethod("cut");

        assertEquals("java.lang.String",
                cut.getGenericReturnType().getTypeName());
        assertEquals(0,
                cut.getParameterCount());
        assertTrue(Modifier.isPublic(cut.getModifiers()));
    }

    @Test
    void testIronSwordOverrideThrustMethod() throws Exception {
        Method thrust = ironSwordClass.getDeclaredMethod("thrust");

        assertEquals("java.lang.String",
                thrust.getGenericReturnType().getTypeName());
        assertEquals(0,
                thrust.getParameterCount());
        assertTrue(Modifier.isPublic(thrust.getModifiers()));
    }

    @Test
    void testIronSwordOverrideGetNameMethod() throws Exception {
        Method getName = ironSwordClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    void testIronSwordOverrideGetHolderMethod() throws Exception {
        Method getHolderName = ironSwordClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    void testIronSwordCutMethod() throws Exception {
        String cutResult = ironSword.cut();
        assertEquals("Iron Sword cut", cutResult);
    }

    @Test
    void testIronSwordThrustMethod() throws Exception {
        String thrustResult = ironSword.thrust();
        assertEquals("Iron Sword thrust", thrustResult);
    }

    @Test
    void testIronSwordGetName() throws Exception {
        String swordName = ironSword.getName();
        assertEquals("Iron Sword", swordName);
    }

    @Test
    void testIronSwordGetHolderName() throws Exception {
        String holderName = ironSword.getHolderName();
        assertEquals("Iron Sword Holder", holderName);
    }
}
