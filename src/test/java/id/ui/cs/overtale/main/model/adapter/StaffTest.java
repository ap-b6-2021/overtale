package id.ui.cs.overtale.main.model.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StaffTest {
    private Staff staff;

    @BeforeEach
    void setUp() throws Exception {
        staff = new Staff("staff", 10, 20);
    }

    @Test
    void testStaffGetName() throws Exception {
        String staffName = staff.getName();
        assertEquals("staff", staffName);
    }

    @Test
    void testStaffGetDamage() throws Exception {
        int staffDamage = staff.getDamage();
        assertEquals(10, staffDamage);
    }

    @Test
    void testStaffGetManaUsage() throws Exception {
        int staffManaUsage = staff.getManaUsage();
        assertEquals(20, staffManaUsage);
    }
}
