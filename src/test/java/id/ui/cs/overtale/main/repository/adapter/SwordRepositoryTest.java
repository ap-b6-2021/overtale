package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.sword.IronSword;
import id.ui.cs.overtale.main.core.items.weapons.sword.StoneSword;
import id.ui.cs.overtale.main.core.items.weapons.sword.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
class SwordRepositoryTest {
    private SwordRepository swordRepository;

    @Mock
    private Map<String, Sword> swords;

    private Sword sampleSword;

    @BeforeEach
    void setUp() {
        swordRepository = new SwordRepositoryImpl();
        swords = new HashMap<>();
        sampleSword = new IronSword("Iron Sword");
        swords.put(sampleSword.getName(), sampleSword);
    }

    @Test
    void whenSwordRepoFindAllItShouldReturnSwordList() {
        ReflectionTestUtils.setField(swordRepository, "swords", swords);
        List<Sword> acquiredSwords = swordRepository.findAll();

        assertThat(acquiredSwords).isEqualTo(new ArrayList<>(swords.values()));
    }

    @Test
    void whenSwordRepoFindByAliasItShouldReturnSwordList() {
        ReflectionTestUtils.setField(swordRepository, "swords", swords);
        Sword acquiredSword = swordRepository.findByAlias(sampleSword.getName());

        assertThat(acquiredSword).isEqualTo(sampleSword);
    }

    @Test
    void whenSwordRepoSaveItShouldSaveSword() {
        ReflectionTestUtils.setField(swordRepository, "swords", swords);
        Sword newSword = new StoneSword("Stone Sword");
        swordRepository.save(newSword);
        Sword acquiredStaff = swordRepository.findByAlias(newSword.getName());

        assertThat(acquiredStaff).isEqualTo(newSword);
    }
}
