let ws;
let msgChain = {}
let base = $("#ws_container")

let expanded = false
let logInner = $("#log-inner")
let gameLogContainer = $("#log-container")

let myInfo = {}
let myUsername = null

$("#log-expand").click(() => {
    gameLogContainer.css("height", expanded ? "4em" : "20em")
    expanded = !expanded
    $("#log-expand").css("transform", expanded ? "rotate(180deg) translateY(0.5em)" : "rotate(0deg)")
})

function connect() {
    let url = $(".ws_addr").val();
    ws = new WebSocket(url);
    ws.onopen = function () {
        log("Connected!");
        refreshMyInfo();
    };
    ws.onmessage = function (data) {
        let msgObj = JSON.parse(data.data)
        if (msgObj.status === "ERROR") {
            log(msgObj.content)
            return
        }
        for (let key in msgChain) {
            try {
                msgChain[key](msgObj)
            } catch (e) {
                console.error(e)
            }
        }
    };
}

function onMessageREFRESH(msgObj) {
    if (msgObj.type === "REFRESH") {
        announce("")
        base.empty()
        let elem = $.parseHTML(msgObj.content, document, true)
        base.append(elem)
    } else if (msgObj.type === "REFRESH_BATTLE") {
        announce("")
        base.empty()
        alert("Battle Started!")
        let elem = $.parseHTML(msgObj.content, document, true)
        base.append(elem)
    }
}

function disconnect() {
    if (ws != null) {
        ws.close();
        log("Disconnected");
    }
}

function send(message) {
    ws.send(message);
}

function log(message) {
    console.log(message)
    logInner.append(`<p>${message}</p>`)
}

function refreshMyInfo() {
    send("getMyInfo")
    msgChain["refreshMyInfo"] = (msgObj) => {
        if (msgObj.type === "PLAYER_DATA") {
            delete msgChain.refreshMyInfo
            myInfo = msgObj.content
            if (myUsername == null) myUsername = myInfo.username
            else if (myUsername !== myInfo.username) refreshMyInfo()
        }
    }
}

function announce(msg) {
    $("#announce").remove()
    $("body").append($.parseHTML(`<div id="announce" class="d-flex flex-column align-items-center" style="margin-top: 2em; position: absolute; top: 0; left: 50%; transform: translateX(-50%)"><p>${msg}</p></div>`))
}

(() => {
    msgChain["onMessageREFRESH"] = onMessageREFRESH
    connect()
})()