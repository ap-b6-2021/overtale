package id.ui.cs.overtale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvertaleApplication {
    public static void main(String[] args) {
        SpringApplication.run(OvertaleApplication.class, args);
    }
}
