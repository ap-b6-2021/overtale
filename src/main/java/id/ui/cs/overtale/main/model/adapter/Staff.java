package id.ui.cs.overtale.main.model.adapter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "staff")
@Data
@NoArgsConstructor
public class Staff {
    @Id
    @Column(name = "id_staff", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idStaff;

    @Column(name = "name")
    private String name;

    @Column(name = "damage")
    private Integer damage;

    @Column(name = "mana_usage")
    private Integer manaUsage;

    public Staff(String name, Integer damage, Integer manaUsage) {
        this.name = name;
        this.damage = damage;
        this.manaUsage = manaUsage;
    }
}

