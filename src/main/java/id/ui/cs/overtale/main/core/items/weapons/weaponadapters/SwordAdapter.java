package id.ui.cs.overtale.main.core.items.weapons.weaponadapters;

import id.ui.cs.overtale.main.core.items.weapons.sword.Sword;
import id.ui.cs.overtale.main.core.items.weapons.weapon.Weapon;

public class SwordAdapter implements Weapon {

    private Sword sword;

    public SwordAdapter(Sword sword) {
        this.sword = sword;
    }

    @Override
    public String normalAttack() {
        return sword.cut();
    }

    @Override
    public String chargedAttack() {
        return sword.thrust();
    }

    @Override
    public String getName() {
        return sword.getName();
    }

    @Override
    public String getHolderName() {
        return sword.getHolderName();
    }
}
