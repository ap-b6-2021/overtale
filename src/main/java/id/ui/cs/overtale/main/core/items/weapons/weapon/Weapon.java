package id.ui.cs.overtale.main.core.items.weapons.weapon;

import id.ui.cs.overtale.main.core.items.Item;

public interface Weapon extends Item {

    String normalAttack();
    String chargedAttack();
    String getHolderName();
}
