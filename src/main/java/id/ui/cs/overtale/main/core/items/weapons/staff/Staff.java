package id.ui.cs.overtale.main.core.items.weapons.staff;

public interface Staff {
    String rechargeMana();
    String cast();
    String getName();
    String getHolderName();
}
