package id.ui.cs.overtale.main.model.adapter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "consumables")
@Data
@NoArgsConstructor
public class Consumables {
    @Id
    @Column(name = "id_consum", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idConsum;

    @Column(name = "name")
    private String name;

    @Column(name = "defense")
    private Integer defense;

    @Column(name = "agility")
    private Integer agility;

    @Column(name = "mana")
    private Integer mana;

    @Column(name = "health_regen")
    private Integer healthRegen;

    @Column(name = "mana_regen")
    private Integer manaRegen;

    public Consumables(String name, Integer defense, Integer agility, Integer mana,
                       Integer healthRegen, Integer manaRegen) {
        this.name = name;
        this.defense = defense;
        this.agility = agility;
        this.mana = mana;
        this.healthRegen = healthRegen;
        this.manaRegen = manaRegen;
    }
}

