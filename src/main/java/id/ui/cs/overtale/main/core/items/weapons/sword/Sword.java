package id.ui.cs.overtale.main.core.items.weapons.sword;

public interface Sword {
    String cut();
    String thrust();
    String getName();
    String getHolderName();
}
