package id.ui.cs.overtale.main.core.items;

import id.ui.cs.overtale.main.core.meta.StatsModifier;
import lombok.Data;

@Data
public class Wearables implements Item, StatsModifier {
    private String name;

    private int maxHealth;
    private int healthRegen;
    private int maxMana;
    private int manaRegen;
    private int defense;
    private int agility;
    private int attackDamage;
}
