package id.ui.cs.overtale.main.core.meta.classes;

import lombok.Getter;

public class Knight extends PlayerClass {
    int defense = 10;

    @Override
    public int getDefense() {
        return defense;
    }
}
