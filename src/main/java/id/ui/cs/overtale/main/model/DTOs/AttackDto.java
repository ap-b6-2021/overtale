package id.ui.cs.overtale.main.model.DTOs;

import lombok.Data;

import java.util.List;

@Data
public class AttackDto {
    EntityDto from;
    List<EntityDto> to;
    List<StatsModifierDto> modifier;
    List<DamageDto> attackDtos;
}
