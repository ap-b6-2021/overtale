package id.ui.cs.overtale.main.core.items;

import id.ui.cs.overtale.main.core.meta.StatsModifier;
import lombok.Data;

@Data
public class Consumables implements Item, StatsModifier {
    private String name;
    private int health;
}
