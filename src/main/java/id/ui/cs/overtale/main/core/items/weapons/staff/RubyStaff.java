package id.ui.cs.overtale.main.core.items.weapons.staff;

public class RubyStaff  implements Staff {
    private String holderName;

    public RubyStaff(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String rechargeMana() {
        return "Ruby Staff recharge mana";
    }

    @Override
    public String cast() {
        return "Ruby Staff cast";
    }

    @Override
    public String getName() {
        return "Ruby Staff";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
