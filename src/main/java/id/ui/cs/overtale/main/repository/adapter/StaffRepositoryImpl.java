package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.staff.Staff;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class StaffRepositoryImpl implements StaffRepository {

    private Map<String, Staff> staffs = new HashMap<>();

    @Override
    public List<Staff> findAll() {
        return new ArrayList<>(staffs.values());
    }

    @Override
    public void save(Staff staff) {
        String staffName = staff.getName();
        staffs.put(staffName, staff);
    }

    @Override
    public Staff findByAlias(String name) {
        return staffs.get(name);
    }
}
