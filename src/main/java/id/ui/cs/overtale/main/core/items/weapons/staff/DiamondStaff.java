package id.ui.cs.overtale.main.core.items.weapons.staff;

public class DiamondStaff implements Staff {

    private String holderName;

    public DiamondStaff(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String rechargeMana() {
        return "Diamond Staff recharge mana";
    }

    @Override
    public String cast() {
        return "Diamond Staff cast";
    }

    @Override
    public String getName() {
        return "Diamond Staff";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
