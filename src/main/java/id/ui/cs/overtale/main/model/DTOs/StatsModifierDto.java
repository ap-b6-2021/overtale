package id.ui.cs.overtale.main.model.DTOs;

import lombok.Data;

@Data
public class StatsModifierDto {
    private int maxHealth;

    private int health;

    private int attackDamage;

    private int defense;

    private int agility;

    private int maxMana;

    private int mana;

    private int healthRegen;

    private int manaRegen;
}
