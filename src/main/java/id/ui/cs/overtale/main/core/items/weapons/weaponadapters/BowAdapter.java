package id.ui.cs.overtale.main.core.items.weapons.weaponadapters;

import id.ui.cs.overtale.main.core.items.weapons.bow.Bow;
import id.ui.cs.overtale.main.core.items.weapons.weapon.Weapon;
import id.ui.cs.overtale.main.core.meta.StatsModifier;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        isAimShot = !isAimShot;
        if (isAimShot) {
            return "Enter Aim Mode";
        } else {
            return "Leave Aim Mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
