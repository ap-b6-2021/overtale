package id.ui.cs.overtale.main.model.adapter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "bow")
@Data
@NoArgsConstructor
public class Bow {
    @Id
    @Column(name = "id_bow", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idBow;

    @Column(name = "name")
    private String name;

    @Column(name = "damage")
    private Integer damage;

    public Bow(String name, Integer damage) {
        this.name = name;
        this.damage = damage;
    }
}

