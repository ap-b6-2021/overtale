package id.ui.cs.overtale.main.core.meta.classes;

public class Mage extends PlayerClass {
    int manaMax = 10;
    int manaRegen = 2;

    @Override
    public int getMaxMana() {
        return manaMax;
    }

    @Override
    public int getManaRegen() {
        return manaRegen;
    }
}
