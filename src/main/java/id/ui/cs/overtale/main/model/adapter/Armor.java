package id.ui.cs.overtale.main.model.adapter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "armor")
@Data
@NoArgsConstructor
public class Armor {
    @Id
    @Column(name = "id_armor", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idArmor;

    @Column(name = "name")
    private String name;

    @Column(name = "defense")
    private Integer defense;

    @Column(name = "agility")
    private Integer agility;

    @Column(name = "mana")
    private Integer mana;

    public Armor(String name, Integer defense, Integer agility, Integer mana) {
        this.name = name;
        this.defense = defense;
        this.agility = agility;
        this.mana = mana;
    }
}

