package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.staff.Staff;

import java.util.List;

public interface StaffRepository {

    List<Staff> findAll();
    void save(Staff staff);
    Staff findByAlias(String name);
}

