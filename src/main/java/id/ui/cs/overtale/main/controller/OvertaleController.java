package id.ui.cs.overtale.main.controller;

import id.ui.cs.overtale.websocket.config.WebSocketConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class OvertaleController {

    @Autowired
    private Environment env;

    @GetMapping("")
    public String home(HttpServletResponse response, Model model) {
        if (isNotAuthenticated()) {
            response.addCookie(new Cookie("jwttoken", ""));
            return "home";
        };
        model.addAttribute("ws_addr", env.getProperty("ws_end")+ WebSocketConfig.WS_URI);
        return "main/base";
    }

    private boolean isNotAuthenticated() {
        var auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null || auth instanceof AnonymousAuthenticationToken;
    }
}
