package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.sword.Sword;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SwordRepositoryImpl implements SwordRepository {

    private Map<String, Sword> swords = new HashMap<>();

    @Override
    public List<Sword> findAll() {
        return new ArrayList<>(swords.values());
    }

    @Override
    public void save(Sword sword) {
        String swordName = sword.getName();
        swords.put(swordName, sword);
    }

    @Override
    public Sword findByAlias(String name) {
        return swords.get(name);
    }
}
