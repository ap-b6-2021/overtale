package id.ui.cs.overtale.main.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "player_inventory")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class PlayerInventory {
    @Id
    @Column(name = "id", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "player_inven")
    @JsonIgnore
    @NonNull
    private Player player;

    @Column(name = "name")
    @NonNull
    private String name;
}