package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.sword.Sword;

import java.util.List;

public interface SwordRepository {

    List<Sword> findAll();
    void save(Sword sword);
    Sword findByAlias(String name);
}

