package id.ui.cs.overtale.main.core.items.weapons.sword;

public class StoneSword implements Sword {

    private String holderName;

    public StoneSword(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String cut() {
        return "Stone Sword cut";
    }

    @Override
    public String thrust() {
        return "Stone Sword thrust";
    }

    @Override
    public String getName() {
        return "Stone Sword";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
