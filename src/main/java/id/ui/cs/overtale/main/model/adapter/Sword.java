package id.ui.cs.overtale.main.model.adapter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sword")
@Data
@NoArgsConstructor
public class Sword {
    @Id
    @Column(name = "id_sword", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSword;

    @Column(name = "name")
    private String name;

    @Column(name = "damage")
    private Integer damage;

    public Sword(String name, Integer damage) {
        this.name = name;
        this.damage = damage;
    }
}

