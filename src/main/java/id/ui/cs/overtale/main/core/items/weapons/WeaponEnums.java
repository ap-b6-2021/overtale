package id.ui.cs.overtale.main.core.items.weapons;

public enum WeaponEnums {
    URANOS_BOW, DIAMOND_STAFF, RUBY_STAFF, IRON_SWORD, STONE_SWORD
}
