package id.ui.cs.overtale.main.core.items;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface Item {
    @JsonProperty("name")
    String getName();
}
