package id.ui.cs.overtale.main.model.DTOs;

import lombok.Data;

@Data
public class DamageDto {
    int damage;
    boolean crit;
}