package id.ui.cs.overtale.main.repository.adapter;

import id.ui.cs.overtale.main.core.items.weapons.bow.Bow;

import java.util.List;

public interface BowRepository {

    List<Bow> findAll();
    void save(Bow bow);
    Bow findByAlias(String name);
}

