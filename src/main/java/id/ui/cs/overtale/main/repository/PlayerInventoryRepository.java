package id.ui.cs.overtale.main.repository;

import id.ui.cs.overtale.main.model.Player;
import id.ui.cs.overtale.main.model.PlayerInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerInventoryRepository extends JpaRepository<PlayerInventory, Integer> {
    PlayerInventory findAllByNameAndPlayer(String name, Player player);
}
