package id.ui.cs.overtale.main.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.ui.cs.overtale.main.core.items.ItemParser;
import id.ui.cs.overtale.main.core.items.Wearables;
import id.ui.cs.overtale.main.core.meta.PlayerStates;
import id.ui.cs.overtale.main.core.meta.StatsModifier;
import id.ui.cs.overtale.main.core.meta.classes.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "player")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Player {
    @Id
    @Column(name = "username")
    @NonNull
    private String username;

    @Column(name = "name")
    @NonNull
    private String name;

    @Column(name = "exp")
    @NonNull
    private Long exp;

    @Column(name = "max_health")
    @NonNull
    private Integer maxHealth;

    @Column(name = "health_regen")
    @NonNull
    private Integer healthRegen;

    @Column(name = "max_mana")
    @NonNull
    private Integer maxMana;

    @Column(name = "mana_regen")
    @NonNull
    private Integer manaRegen;

    @Column(name = "attack_damage")
    @NonNull
    private Integer attackDamage;

    @Column(name = "defense")
    @NonNull
    private Integer defense;

    @Column(name = "agility")
    @NonNull
    private Integer agility;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "player_inven")
    @JsonProperty("inventory")
    private List<PlayerInventory> playerInventories = new ArrayList<>();

    @Column(name = "player_state")
    @Enumerated(EnumType.ORDINAL)
    @JsonIgnore
    @NonNull
    private PlayerStates playerState;

    @Column(name = "player_class")
    @Enumerated(EnumType.ORDINAL)
    private PlayerClassEnum playerClass;

    @Transient
    @JsonIgnore
    private PlayerClass playerClassInstance;

    @Column(name = "player_armor")
    @JsonIgnore
    private String armor;

    @Transient
    @JsonProperty("armor")
    private Wearables armorInstance;

    @Column(name = "player_weapon")
    @JsonIgnore
    private String weapon;

    @Transient
    @JsonProperty("weapon")
    private Wearables weaponInstance;

    public Player (String username, String name) {
        this(username, name, 100L, 100, 1, 0, 0, 10, 10, 0,  PlayerStates.PRECLASS);
    }

    @JsonIgnore
    public Player getPlayerDto() {
        var player = new Player(this.username, this.name, this.exp, this.maxHealth, this.healthRegen, this.maxMana, this.manaRegen, this.attackDamage, this.defense, this.agility, this.playerState);
        player.playerInventories = this.playerInventories;
        player.playerClass = this.playerClass;
        player.armor = this.armor;
        player.weapon = this.weapon;
        player.fillEnums();

        if (playerClassInstance != null) {
            appendStat(player, playerClassInstance);
        }
        if (armorInstance != null) {
            appendStat(player, armorInstance);
        }

        return player;
    }

    private void appendStat(Player player, StatsModifier statsModifier) {
        player.attackDamage += statsModifier.getAttackDamage();
        player.defense += statsModifier.getDefense();
        player.agility += statsModifier.getAgility();
        player.maxHealth += statsModifier.getMaxHealth();
        player.maxHealth += statsModifier.getMaxHealth();
        player.maxMana += statsModifier.getMaxMana();
        player.healthRegen += statsModifier.getHealthRegen();
        player.manaRegen += statsModifier.getManaRegen();
    }

    @PostLoad
    public void fillEnums() {
        var parser = ItemParser.getInstance();
        if (playerClass != null) {
            switch (playerClass) {
                case MAGE:
                    setPlayerClassInstance(new Mage());
                    break;
                case KNIGHT:
                    setPlayerClassInstance(new Knight());
                    break;
                case ROGUE:
                    setPlayerClassInstance(new Rogue());
                    break;
                default:
            }
        }
        if (armor != null) {
            armorInstance = parser.getWearablesMap().get(armor);
        }
        if (weapon != null) {
            weaponInstance = parser.getWearablesMap().get(weapon);
        }
    }
}