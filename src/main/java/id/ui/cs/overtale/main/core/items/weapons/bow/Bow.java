package id.ui.cs.overtale.main.core.items.weapons.bow;

public interface Bow {
    String shootArrow(boolean isAimShot);
    String getName();
    String getHolderName();
}
