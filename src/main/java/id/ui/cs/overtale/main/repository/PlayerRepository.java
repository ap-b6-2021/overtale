package id.ui.cs.overtale.main.repository;

import id.ui.cs.overtale.main.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
    Player findPlayerByUsername(String username);
    Player findPlayerByName(String name);
}
