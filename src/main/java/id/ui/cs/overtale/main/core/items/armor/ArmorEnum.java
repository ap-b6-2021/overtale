package id.ui.cs.overtale.main.core.items.armor;

public enum ArmorEnum {
    IRON_ARMOR, WIZARD_ROBE, FLEETING_ARMOR
}
