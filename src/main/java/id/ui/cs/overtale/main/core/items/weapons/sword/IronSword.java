package id.ui.cs.overtale.main.core.items.weapons.sword;

public class IronSword implements Sword {

    private String holderName;

    public IronSword(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String cut() {
        return "Iron Sword cut";
    }

    @Override
    public String thrust() {
        return "Iron Sword thrust";
    }

    @Override
    public String getName() {
        return "Iron Sword";
    }

    @Override
    public String getHolderName() {
        return holderName;
    }
}
