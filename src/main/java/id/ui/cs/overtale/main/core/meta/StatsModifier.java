package id.ui.cs.overtale.main.core.meta;

public interface StatsModifier {

    default int getMaxHealth() {
        return 0;
    }

    default int getHealthRegen() {
        return 0;
    }

    default int getMana() {
        return 0;
    }

    default int getMaxMana() {
        return 0;
    }

    default int getManaRegen() {
        return 0;
    }

    default int getDefense() {
        return 0;
    }

    default int getAgility() {
        return 0;
    }

    default int getAttackDamage() {
        return 0;
    }
}
