package id.ui.cs.overtale.main.model.DTOs;

import lombok.Data;

import java.util.List;

@Data
public class EntityDto {
    EntityStateEnum entityState;
    List<StatsModifierDto> statsModifiers;
    int health;
}
