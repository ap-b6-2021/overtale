package id.ui.cs.overtale.main.core.meta.classes;

public class Rogue extends PlayerClass {

    int agility = 10;

    @Override
    public int getAgility() {
        return agility;
    }
}
