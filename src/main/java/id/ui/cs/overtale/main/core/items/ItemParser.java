package id.ui.cs.overtale.main.core.items;

import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLParser;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Eager instantiation approach
 */
@Data
@Slf4j
public class ItemParser {

    private static ItemParser instance = new ItemParser();

    @Getter(value = AccessLevel.NONE)
    @Setter(value = AccessLevel.NONE)
    private YAMLFactory factory;

    @Getter(value = AccessLevel.NONE)
    @Setter(value = AccessLevel.NONE)
    private ObjectMapper mapper;

    private Map<String, Consumables> consumablesMap;
    private Map<String, Wearables> wearablesMap;

    private ItemParser() {
        factory = new YAMLFactory();
        mapper = new ObjectMapper(factory);
        mapper.findAndRegisterModules();
        consumablesMap = new HashMap<>();
        wearablesMap = new HashMap<>();
        try {
            parse();
        } catch (Exception e) {
            log.error("ItemParser Error:", e);
        }
    }

    /**
     * Karena itemParser sudah dibuat di awal, langsung mengembalikan
     * attribute itemParser.
     */
    public static ItemParser getInstance() {
        return instance;
    }

    public void parse() throws IOException {
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("item-list.yaml");
        try (YAMLParser parser = factory.createParser(stream)) {
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
                    continue;
                }

                // skip everything until a field name
                while (parser.nextToken() != JsonToken.FIELD_NAME) ;

                Class<? extends Item> type = getType(parser.getCurrentName());
                if (type == null) {
                    continue;
                }

                // skip field name
                parser.nextToken();
                parser.nextToken();

                insert(type, mapper.readValue(parser, type));
            }
        }
    }

    private Class<? extends Item> getType(@NonNull String fieldName) {
        switch (fieldName) {
            case "wearables":
                return Wearables.class;
            case "consumables":
                return Consumables.class;
            default:
                return null;
        }
    }

    private void insert(@NonNull Class<? extends Item> classType, Item item) {
        if (classType.equals(Consumables.class)) {
            consumablesMap.put(item.getName(), (Consumables) item);
        } else if (classType.equals(Wearables.class)) {
            wearablesMap.put(item.getName(), (Wearables) item);
        }
    }
}
