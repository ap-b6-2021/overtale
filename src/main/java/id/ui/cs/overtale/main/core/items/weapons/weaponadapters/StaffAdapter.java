package id.ui.cs.overtale.main.core.items.weapons.weaponadapters;

import id.ui.cs.overtale.main.core.items.weapons.staff.Staff;
import id.ui.cs.overtale.main.core.items.weapons.weapon.Weapon;

public class StaffAdapter implements Weapon {

    private Staff staff;

    public StaffAdapter(Staff staff) {
        this.staff = staff;
    }

    @Override
    public String normalAttack() {
        return staff.cast();
    }

    @Override
    public String chargedAttack() {
        return staff.rechargeMana();
    }

    @Override
    public String getName() {
        return staff.getName();
    }

    @Override
    public String getHolderName() {
        return staff.getHolderName();
    }
}
