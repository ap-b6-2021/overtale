package id.ui.cs.overtale.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class JwtRequestRegister implements Serializable {

    private static final long serialVersionUID = 2934768384655150707L;

    private String username;
    private String email;
    private String password;
    private String matchingPassword;
}
