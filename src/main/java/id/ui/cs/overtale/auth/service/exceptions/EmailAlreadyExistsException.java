package id.ui.cs.overtale.auth.service.exceptions;

public class EmailAlreadyExistsException extends Exception {
    public EmailAlreadyExistsException(String msg) {
        super(msg);
    }
}
