package id.ui.cs.overtale.auth.service;

import id.ui.cs.overtale.auth.model.JwtRequest;
import id.ui.cs.overtale.auth.model.JwtRequestRegister;
import id.ui.cs.overtale.auth.model.User;
import id.ui.cs.overtale.auth.service.exceptions.ConfirmPasswordFailed;
import id.ui.cs.overtale.auth.service.exceptions.EmailAlreadyExistsException;
import id.ui.cs.overtale.auth.service.exceptions.UserAlreadyExistsException;
import org.springframework.security.authentication.BadCredentialsException;

public interface UserService {
    User register(JwtRequestRegister request) throws UserAlreadyExistsException, ConfirmPasswordFailed, EmailAlreadyExistsException;
    String login(JwtRequest request) throws BadCredentialsException;
    void logout();
    User getUserByUsername(String username);
    void save(User user);
}
