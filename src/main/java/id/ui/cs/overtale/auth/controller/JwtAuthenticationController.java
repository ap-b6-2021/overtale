package id.ui.cs.overtale.auth.controller;

import id.ui.cs.overtale.auth.model.JwtRequest;
import id.ui.cs.overtale.auth.model.JwtRequestRegister;
import id.ui.cs.overtale.auth.service.UserService;
import id.ui.cs.overtale.auth.service.exceptions.ConfirmPasswordFailed;
import id.ui.cs.overtale.auth.service.exceptions.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class JwtAuthenticationController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/login")
    public String login(ServletRequest request, HttpServletResponse response) {
        if (request.getParameterMap().containsKey("logout") || !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
            response.addCookie(new Cookie("jwttoken", ""));
        }
        return "auth/login";
    }

    @PostMapping(value = "/login")
    public String login(HttpServletResponse response, Model model, @ModelAttribute("user") JwtRequest request) {
        List<String> errMsg = new ArrayList<>();
        String token;
        try {
            token = userService.login(request);
        } catch (BadCredentialsException e) {
            errMsg.add(e.getMessage());
            model.addAttribute("err_msg", errMsg);
            return "auth/login";
        }
        response.addCookie(new Cookie("jwttoken", token));
        return "redirect:/";
    }

    @GetMapping(value = "/register")
    public String register() {
        return "auth/register";
    }

    @PostMapping(value = "/register")
    public String register(Model model, @ModelAttribute("user") JwtRequestRegister request) {
        List<String> errMsg = new ArrayList<>();
        try {
            userService.register(request);
        } catch (Exception e) {
            errMsg.add(e.getMessage());
            model.addAttribute("err_msg", errMsg);
            return "auth/register";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletResponse servletResponse) {
        userService.logout();
        servletResponse.addCookie(new Cookie("jwttoken", "m"));
        return "redirect:/login";
    }
}
