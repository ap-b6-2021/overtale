package id.ui.cs.overtale.auth.service;

import id.ui.cs.overtale.auth.config.JwtTokenUtil;
import id.ui.cs.overtale.auth.model.JwtRequest;
import id.ui.cs.overtale.auth.model.JwtRequestRegister;
import id.ui.cs.overtale.auth.model.User;
import id.ui.cs.overtale.auth.repository.UserRepository;
import id.ui.cs.overtale.auth.service.exceptions.ConfirmPasswordFailed;
import id.ui.cs.overtale.auth.service.exceptions.EmailAlreadyExistsException;
import id.ui.cs.overtale.auth.service.exceptions.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;

    @Override
    public User register(JwtRequestRegister request) throws UserAlreadyExistsException, ConfirmPasswordFailed, EmailAlreadyExistsException {
        if (!request.getMatchingPassword().equals(request.getPassword())) throw new ConfirmPasswordFailed("Password didn't match.");
        if (userRepository.findByUsername(request.getUsername()) != null) {
            throw new UserAlreadyExistsException(String.format("Username %s already exists!", request.getUsername()));
        }
        if (userRepository.findByEmail(request.getEmail()) != null) {
            throw new EmailAlreadyExistsException(String.format("Email %s already registered!", request.getEmail()));
        }
        var user = new User(request.getUsername(), request.getEmail(), encoder.encode(request.getPassword()));
        userRepository.save(user);
        return user;
    }

    @Override
    public String login(JwtRequest request) throws BadCredentialsException {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
        return jwtTokenUtil.generateToken(userDetails);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
