package id.ui.cs.overtale.auth.model;

import id.ui.cs.overtale.main.model.Player;
import lombok.*;
import org.springframework.web.socket.WebSocketSession;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_player", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class User implements Serializable {

    @Id
    @Column(name = "username", nullable = false, unique = true)
    @NonNull
    private String username;

    @Column(name = "email", nullable = false, unique = true)
    @NonNull
    private String email;

    @Column(name = "password", nullable = false)
    @NonNull
    private String password;

    @JoinColumn(name = "player")
    @ManyToOne
    private Player player;

    @Transient
    private WebSocketSession session;
}
