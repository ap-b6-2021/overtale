package id.ui.cs.overtale.auth.service.exceptions;

public class ConfirmPasswordFailed extends Exception{
    public ConfirmPasswordFailed(String msg) {
        super(msg);
    }
}
