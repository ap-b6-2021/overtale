package id.ui.cs.overtale.websocket.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ui.cs.overtale.auth.config.JwtTokenUtil;
import id.ui.cs.overtale.auth.model.User;
import id.ui.cs.overtale.auth.service.UserService;
import id.ui.cs.overtale.main.core.items.Consumables;
import id.ui.cs.overtale.main.core.items.ItemParser;
import id.ui.cs.overtale.main.core.meta.PlayerStates;
import id.ui.cs.overtale.main.core.meta.classes.PlayerClassEnum;
import id.ui.cs.overtale.main.model.DTOs.AttackDto;
import id.ui.cs.overtale.main.model.Player;
import id.ui.cs.overtale.main.model.PlayerInventory;
import id.ui.cs.overtale.websocket.service.PlayerService;
import id.ui.cs.overtale.websocket.service.WebClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class BattleWebSocketHandler extends TextWebSocketHandler {
    private final Map<WebSocketSession, User> sessionMap = new ConcurrentHashMap<>();
    private final Map<String, WebSocketSession> userMap = new ConcurrentHashMap<>();

    private ObjectMapper mapper = new ObjectMapper();

    private static final String INVALID_SYNTAX = "Invalid syntax";
    private static final String COMMAND_NOT_FOUND = "Command not found";
    private static final String STATUS = "status";
    private static final String CONTENT = "content";
    private static final String REASON = "reason";
    private static final String ERROR = "error";
    private static final List<String> CMDS = Arrays.asList("getParties", "getParty", "createParty", "joinParty",
                                                           "disbandPlayer", "disbandParty", "startBattle",
                                                           "attack", "setClass", "getMyInfo", "leaveParty",
                                                           "startBattle", "getMyInfoParty", "upStat", "getMenuFromBattle",
                                                           "useItem");

    @Autowired
    WebClientService webClientService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private PlayerService playerService;

    @Value("classpath:templates/main/class.html")
    Resource classTemplate;

    @Value("classpath:templates/main/menu.html")
    Resource menuTemplate;

    @Value("classpath:templates/main/battle.html")
    Resource battleTemplate;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);

        String token = null;
        try {
            String[] cookies = session.getHandshakeHeaders().get(HttpHeaders.COOKIE).get(0).split(";");
            for (String cookie : cookies) {
                String[] keyVal = cookie.strip().split("=");
                if (keyVal[0].equals("jwttoken")) token = keyVal[1];
            }
        } catch (NullPointerException e) {
            log.error(e.toString());
        }

        String username = jwtTokenUtil.getUsernameFromToken(token);

        var user = userService.getUserByUsername(username);
        var player = playerService.getPlayer(user.getUsername());
        user.setPlayer(player);
        userService.save(user);

        sessionMap.put(session, user);
        userMap.put(user.getUsername(), session);

        if (player.getPlayerState().equals(PlayerStates.PRECLASS)) {
            sendSelectClass(session);
        } else {
            sendMenu(session);
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        var user = sessionMap.get(session);
        user.setSession(null);
        sessionMap.remove(session);
        userMap.remove(user.getUsername());
        super.afterConnectionClosed(session, status);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        log.info("Websocket - {}: {}", sessionMap.get(session).getUsername(), message.getPayload());
        String[] queries = message.getPayload().split(" ");
        if (!CMDS.contains(queries[0])) {
            sendResponseError(session, COMMAND_NOT_FOUND);
            return;
        }
        try {
            var method = this.getClass().getDeclaredMethod(queries[0], WebSocketSession.class, String[].class);
            method.invoke(this, session, queries);
        } catch (InvocationTargetException e) {
            Throwable target = e.getTargetException();
            if (target instanceof WebClientResponseException) {
                var response = ((WebClientResponseException) target).getResponseBodyAsString();
                sendResponseError(session, response);
            } else throw e;
        }
        catch (Exception e) {
            log.error("Error", e);
            sendResponseError(session, "Internal Server Error");
        }

    }

    private Player getPlayer(WebSocketSession session) {
        User player = sessionMap.get(session);
        Player player1 = playerService.getPlayer(player.getPlayer().getUsername());
        player.setPlayer(player1);
        return player1;
    }

    private void sendToSessionMap(WebSocketSession session, Map<String, ? extends Object> map) {
        try {
            sendToSession(session, mapper.writeValueAsString(map));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void sendToSession(WebSocketSession session, String msg) {
        try {
            session.sendMessage(new TextMessage(msg));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendResponseOk(WebSocketSession session, String type, Object content) {
        Map<String, Object> respObj = new HashMap<>();
        respObj.put("status", "OK");
        respObj.put("type", type);
        respObj.put("content", content);
        sendToSessionMap(session, respObj);
    }

    private void sendResponseError(WebSocketSession session, String reason) {
        Map<String, Object> respObj = new HashMap<>();
        respObj.put("status", "ERROR");
        respObj.put("content", reason);
        sendToSessionMap(session, respObj);
    }

    private void sendSelectClass(WebSocketSession session) {
        String template;
        try {
            template = new String(classTemplate.getInputStream().readAllBytes());
            sendResponseOk(session, "REFRESH", template);
        } catch (IOException e) {
            log.error("Error: ", e);
            sendResponseError(session, "Internal Server Error");
        }
    }

    private void sendMenu(WebSocketSession session) {
        String template;
        try {
            template = new String(menuTemplate.getInputStream().readAllBytes());
            sendResponseOk(session, "REFRESH", template);
        } catch (IOException e) {
            log.error("Error: ", e);
            sendResponseError(session, "Internal Server Error");
        }
    }

    private void sendBattle(WebSocketSession session) {
        String template;
        try {
            template = new String(battleTemplate.getInputStream().readAllBytes());
            sendResponseOk(session, "REFRESH_BATTLE", template);
        } catch (IOException e) {
            log.error("Error: ", e);
            sendResponseError(session, "Internal Server Error");
        }
    }

    @SuppressWarnings("Unchecked")
    @Async
    void setClass(WebSocketSession session, String[] queries) {
        if (queries.length != 2) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        Player player = getPlayer(session);
        player.setPlayerClass(PlayerClassEnum.valueOf(queries[1]));
        switch (player.getPlayerClass()) {
            case MAGE:
                player.setArmor("Wizard Robe");
                player.setWeapon("Staff of Homa");
                break;
            case ROGUE:
                player.setArmor("Fleeting Armor");
                player.setWeapon("Uranos Bow");
                break;
            case KNIGHT:
                player.setArmor("Iron Armor");
                player.setWeapon("Skyward Blade");
                break;
            default:
        }
        PlayerInventory inventory = new PlayerInventory(player, "Health Potion");
        playerService.savePlayerInventory(inventory);
        player.getPlayerInventories().add(inventory);
        player.setPlayerState(PlayerStates.CLASSED);
        player.fillEnums();
        playerService.savePlayer(player);
        sendMenu(session);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void upStat(WebSocketSession session, String[] queries) {
        if (queries.length != 2) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        Player player = getPlayer(session);
        if(player.getExp() < 50) {
            sendResponseError(session, "You have insufficient EXP!");
            return;
        }
        switch (queries[1]) {
            case "attackDamage":
                player.setAttackDamage(player.getAttackDamage()+1);
                break;
            case "defense":
                player.setDefense(player.getDefense()+1);
                break;
            case "agility":
                player.setAgility(player.getAgility()+1);
                break;
            case "maxHealth":
                player.setMaxHealth(player.getMaxHealth()+1);
                break;
            case "maxMana":
                player.setMaxMana(player.getMaxMana()+1);
                break;
            case "healthRegen":
                player.setHealthRegen(player.getHealthRegen()+1);
                break;
            case "manaRegen":
                player.setManaRegen(player.getManaRegen());
                break;
            default:
                sendResponseError(session, "Unknown stat!");
                return;
        }
        player.setExp(player.getExp()-50);
        playerService.savePlayer(player);
        sendResponseOk(session, "PLAYER_DATA", player.getPlayerDto());
    }

    @SuppressWarnings("Unchecked")
    @Async
    void getMenuFromBattle(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        var player = getPlayer(session);
        ResponseEntity<Object> clientResponse =  webClientService.perform("GET", "/battle/get/reward/" + player.getName()).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        int reward = ((int) ((LinkedHashMap) body).get("exp"));
        player.setExp(player.getExp() + reward);

        sendMenu(session);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void getMyInfo(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        var player = getPlayer(session);
        sendResponseOk(session, "PLAYER_DATA", player.getPlayerDto());
    }

    @SuppressWarnings("Unchecked")
    @Async
    void getMyInfoParty(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        var player = getPlayer(session);
        ResponseEntity<Object> clientResponse =  webClientService.perform("GET", "/party/get/playerInfo/" + player.getName()).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        ((LinkedHashMap<String, Object>) body).put("inventory", player.getPlayerInventories());
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "PLAYER_DATA", body);
        else sendResponseError(session, (String) body);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void getParties(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        ResponseEntity<Object> clientResponse =  webClientService.perform("GET", "/party/get").block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "PARTY_LIST", body);
        else sendResponseError(session, (String) body);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void getParty(WebSocketSession session, String[] queries) {
        if (queries.length > 1) {
            ResponseEntity<Object> clientResponse = webClientService.perform("GET", "/party/get/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length))).block();
            Object body = clientResponse.getBody();
            if (body == null) return;
            if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "PARTY_DATA", body);
            else sendResponseError(session, (String) body);
            return;
        } else if (queries.length == 1) {
            String username = sessionMap.get(session).getUsername();

            ResponseEntity<Object> clientResponse = webClientService.perform("GET", "/party/get/player/" + username).block();
            Object body = clientResponse.getBody();
            if (body == null) return;
            if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "PARTY_DATA", body);
            else sendResponseError(session, (String) body);
            return;
        }


        sendResponseError(session, INVALID_SYNTAX);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void createParty(WebSocketSession session, String[] queries) {
        if (queries.length == 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        var leader = getPlayer(session);

        ResponseEntity<Object> clientResponse = webClientService
                .perform("POST", "/party/create/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length)), leader.getPlayerDto(), Player.class).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "PARTY_DATA", body);
        else sendResponseError(session, (String) body);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void joinParty(WebSocketSession session, String[] queries) {
        if (queries.length == 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        var leader = getPlayer(session);
        ResponseEntity<Object> clientResponse = webClientService
                .perform("PUT", "/party/add/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length)), leader.getPlayerDto(), Player.class).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) bcPartyInfo(queries[1]);
        else sendResponseError(session, (String) body);
    }

    private void bcPartyInfo(String partyName) {
        ResponseEntity<Object> clientResponse = webClientService.perform("GET", "/party/get/" + partyName).block();
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) {
            LinkedHashMap resp = (LinkedHashMap) clientResponse.getBody();
            List<LinkedHashMap> players = (List<LinkedHashMap>) resp.get("players");
            for (LinkedHashMap player : players) {
                LinkedHashMap playerObj = (LinkedHashMap) player.get("player");
                String pName = (String) playerObj.get("name");
                User user = userService.getUserByUsername(pName);
                WebSocketSession session = userMap.get(user.getUsername());
                if (session == null) continue;
                sendResponseOk(session, "PARTY_DATA", resp);
            }
        }
        else log.error("bcPartyInfo Error");
    }

    @SuppressWarnings("Unchecked")
    @Async
    void leaveParty(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }
        var player = getPlayer(session);
        ResponseEntity<Object> clientResponse = webClientService
                .perform("DELETE", "/party/disband/player/" + player.getName()).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) {
            sendResponseOk(session, "NO_CONTENT", null);
            bcPartyInfo(((LinkedHashMap<String, String>) body).get("partyName"));
        }
        else sendResponseError(session, (String) body);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void disbandPlayer(WebSocketSession session, String[] queries) {
        if (queries.length == 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        var player = getPlayer(session);
        ResponseEntity<Object> clientResponse = webClientService
                .perform("DELETE", "/party/disband/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length)) + "/" + player.getName()).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) {
            sendResponseOk(session, "NO_CONTENT", null);
            bcPartyInfo(((LinkedHashMap<String, String>) body).get("partyName"));
        }
        else sendResponseError(session, (String) body);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void disbandParty(WebSocketSession session, String[] queries) {
        if (queries.length == 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        ResponseEntity<Object> clientResponse1 =
                webClientService.perform("GET", "/party/get/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length))).block();
        Object body = clientResponse1.getBody();
        if (body == null) return;
        String name = (String) ((LinkedHashMap) ((LinkedHashMap) ((LinkedHashMap) body).get("partyLeader")).get("player")).get("name");
        if (!sessionMap.get(session).getUsername().equals(name)) {
            sendResponseError(session, "You are not the leader of the party!");
            return;
        }

        ResponseEntity<Object> clientResponse = webClientService
                .perform("DELETE", "/party/disband/" + String.join(" ", Arrays.copyOfRange(queries, 1, queries.length))).block();
        Object body1 = clientResponse.getBody();
        if (body1 == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) sendResponseOk(session, "NO_CONTENT", null);
        else sendResponseError(session, (String) body1);
    }

    @SuppressWarnings("Unchecked")
    @Async
    void startBattle(WebSocketSession session, String[] queries) {
        if (queries.length != 1) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        String username = sessionMap.get(session).getUsername();
        ResponseEntity<Object> clientResponse1 = webClientService.perform("GET", "/party/get/player/" + username).block();
        Object body1 = clientResponse1.getBody();
        if (body1 == null) return;
        String name = (String) ((LinkedHashMap) ((LinkedHashMap) ((LinkedHashMap) body1).get("partyLeader")).get("player")).get("name");
        if (!sessionMap.get(session).getUsername().equals(name)) {
            sendResponseError(session, "You are not the leader of the party!");
            return;
        }

        String partyName = ((String) ((LinkedHashMap) body1).get("partyName"));

        List<Player> players = new ArrayList<>();
        for (LinkedHashMap player : ((List<LinkedHashMap>) ((LinkedHashMap) body1).get("players"))) {
            String name1 = ((String) ((LinkedHashMap) player.get("player")).get("name"));
            players.add(playerService.getPlayer(name1).getPlayerDto());
        }

        ResponseEntity<Object> clientResponse = webClientService
                .perform("POST", "/battle/start/" + partyName, players, List.class).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) bcPartyStart(partyName);
        else sendResponseError(session, (String) body);
    }

    private void bcPartyStart(String partyName) {
        ResponseEntity<Object> clientResponse = webClientService.perform("GET", "/party/get/" + partyName).block();
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) {
            LinkedHashMap resp = (LinkedHashMap) clientResponse.getBody();
            List<LinkedHashMap> players = (List<LinkedHashMap>) resp.get("players");
            for (LinkedHashMap player : players) {
                LinkedHashMap playerObj = (LinkedHashMap) player.get("player");
                String pName = (String) playerObj.get("name");
                User user = userService.getUserByUsername(pName);
                WebSocketSession session = userMap.get(user.getUsername());
                if (session == null) continue;
                sendBattle(session);
            }
        }
        else log.error("bcPartyInfo Error");
    }

    @SuppressWarnings("Unchecked")
    @Async
    void attack(WebSocketSession session, String[] queries) {
        if (queries.length != 2) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        String playername = sessionMap.get(session).getUsername();

        var clientResponse = webClientService
                .perform("POST", String.format("/battle/attack/%s/%s", playername, queries[1])).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) bcParty(playername, body, "ATTACK_RESPONSE");
        else sendResponseError(session, ((String) body));
    }

    private void bcParty(String playername,  Object attackInfo, String type) {
        ResponseEntity<Object> clientResponse =  webClientService.perform("GET", "/party/get/player/" + playername).block();
        Object body = clientResponse.getBody();
        if (clientResponse.getStatusCode().equals(HttpStatus.OK)) {
            LinkedHashMap resp = (LinkedHashMap) clientResponse.getBody();
            List<LinkedHashMap> players = (List<LinkedHashMap>) resp.get("players");
            for (LinkedHashMap player : players) {
                LinkedHashMap playerObj = (LinkedHashMap) player.get("player");
                String pName = (String) playerObj.get("name");
                User user = userService.getUserByUsername(pName);
                WebSocketSession session = userMap.get(user.getUsername());
                if (session == null) continue;
                Map<String, Object> respObj = new HashMap<>();
                respObj.put("partyInfo", body);
                respObj.put("attackInfo", attackInfo);
                sendResponseOk(session, type, respObj);
            }
        }
        else log.error("bcPartyInfo Error");
    }

    @SuppressWarnings("Unchecked")
    @Async
    void useItem(WebSocketSession session, String[] queries) {
        if (queries.length == 2) {
            sendResponseError(session, INVALID_SYNTAX);
            return;
        }

        var itemName = String.join(" ", Arrays.copyOfRange(queries, 1, queries.length));

        Player player = getPlayer(session);
        PlayerInventory playerInventory = playerService.getFromNameAndPlayer(itemName, player);
        if (playerInventory == null) {
            sendResponseError(session, "Item doesn't exists!");
        }

        playerService.deletePlayerInventory(playerInventory);

        var clientResponse = webClientService
                .perform("POST", String.format("/battle/buff/%s", player.getName()), ItemParser.getInstance().getConsumablesMap().get(itemName), Consumables.class).block();
        Object body = clientResponse.getBody();
        if (body == null) return;
        ((LinkedHashMap<String, Object>) body).put("player", player.getName());
        bcParty(player.getName(), body, "BUFF_RESPONSE");
    }
}
