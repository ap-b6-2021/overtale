package id.ui.cs.overtale.websocket.config;

import id.ui.cs.overtale.websocket.handler.BattleWebSocketHandler;
import id.ui.cs.overtale.websocket.interceptor.TokenHandshakeInterceptor;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    public static final String WS_URI = "/ws/battle";

    @Autowired
    private Environment env;

    @Autowired
    private BattleWebSocketHandler battleWebSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(battleWebSocketHandler, WS_URI).addInterceptors(getHandshakeInterceptor());
    }

    @Bean
    public TokenHandshakeInterceptor getHandshakeInterceptor() {
        return new TokenHandshakeInterceptor();
    }

    @Bean
    public WebClient webClientFromBuilder(WebClient.Builder webClientBuilder) {

        var httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5_000) // millis
                .doOnConnected(connection ->
                        connection
                                .addHandlerLast(new ReadTimeoutHandler(5)) // seconds
                                .addHandlerLast(new WriteTimeoutHandler(5))); //seconds
        return webClientBuilder
                .baseUrl("http://" + env.getProperty("battle.url", ""))
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.USER_AGENT, "I'm a teapot")
                .build();
    }
}
