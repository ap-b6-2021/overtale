package id.ui.cs.overtale.websocket.service;

import id.ui.cs.overtale.main.model.Player;
import id.ui.cs.overtale.main.model.PlayerInventory;
import id.ui.cs.overtale.main.repository.PlayerInventoryRepository;
import id.ui.cs.overtale.main.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService{

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    PlayerInventoryRepository playerInventoryRepository;

    @Override
    public Player getPlayer(String username) {
        Player player = playerRepository.findPlayerByUsername(username);
        if (player == null) {
            player = new Player(username, username);
            savePlayer(player);
        }
        return player;
    }

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public PlayerInventory savePlayerInventory(PlayerInventory playerInventory) {
        return playerInventoryRepository.save(playerInventory);
    }

    @Override
    public PlayerInventory getFromNameAndPlayer(String name, Player player) {
        return playerInventoryRepository.findAllByNameAndPlayer(name, player);
    }

    @Override
    public void deletePlayerInventory(PlayerInventory playerInventory) {
        List<PlayerInventory> inven = playerInventory.getPlayer().getPlayerInventories();
        for (int i = 0; i < inven.size(); i++) {
            if (inven.get(i).getId().equals(playerInventory.getId())) {
                inven.remove(i);
                break;
            }
        }
        playerRepository.save(playerInventory.getPlayer());
    }
}
