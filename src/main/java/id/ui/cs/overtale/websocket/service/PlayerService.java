package id.ui.cs.overtale.websocket.service;

import id.ui.cs.overtale.main.model.Player;
import id.ui.cs.overtale.main.model.PlayerInventory;

public interface PlayerService {
    Player getPlayer(String username);
    Player savePlayer(Player player);
    PlayerInventory savePlayerInventory(PlayerInventory playerInventory);
    PlayerInventory getFromNameAndPlayer(String name, Player player);
    public void deletePlayerInventory(PlayerInventory playerInventory);
}
