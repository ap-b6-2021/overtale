package id.ui.cs.overtale.websocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WebClientServiceImpl implements WebClientService{
    @Autowired
    WebClient webClient;

    private WebClient.RequestHeadersUriSpec<?> buildHeader(String method) {
        switch (method) {
            case "GET":
                return webClient.get();
            case "POST":
                return webClient.post();
            case "PUT":
                return webClient.put();
            case "DELETE":
                return webClient.delete();
            default:
                return null;
        }
    }

    @Override
    public Mono<ResponseEntity<Object>> perform(String method, String uri) {
        WebClient.RequestHeadersUriSpec<?> header = buildHeader(method);
        if (header == null) return null;
        return header.uri(uri).retrieve().toEntity(Object.class);
    }

    @Override
    public Mono<ResponseEntity<Object>> perform(String method, String uri, Object body, Class<?> classType) {
        WebClient.RequestHeadersUriSpec<?> header = buildHeader(method);
        if (header == null) return null;
        return ((WebClient.RequestBodyUriSpec) header).uri(uri).body(Mono.just(body), classType).retrieve()
                .toEntity(Object.class);
    }

    @Override
    public <T> Mono<ResponseEntity<T>> performMono(String method, String uri, ParameterizedTypeReference<T> monoClass) {
        WebClient.RequestHeadersUriSpec<?> header = buildHeader(method);
        if (header == null) return null;
        return header.uri(uri).retrieve().toEntity(monoClass);
    }


}
