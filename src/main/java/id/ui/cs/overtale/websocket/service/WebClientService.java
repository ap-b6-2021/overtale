package id.ui.cs.overtale.websocket.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

public interface WebClientService {
    Mono<ResponseEntity<Object>> perform(String method, String uri);

    Mono<ResponseEntity<Object>> perform(String method, String uri, Object body, Class<?> classType);

    <T> Mono<ResponseEntity<T>> performMono(String method, String uri, ParameterizedTypeReference<T> monoClass);
}
